import { Component } from '@angular/core';

// interfaz para el Menu
interface MenuItem {
  texto: string;
  ruta: string;
}

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.css'],
})
export class SidemenuComponent {
  public menuTemplate: MenuItem[] = [
    {
      texto: 'Basicos',
      ruta: './template/basicos',
    },
    {
      texto: 'Dinamicos',
      ruta: './template/dinamicos',
    },
    {
      texto: 'Switches',
      ruta: './template/switches',
    },
  ];

  public MenuReactive: MenuItem[] = [
    {
      texto: 'Basicos',
      ruta: './reactive/basicos',
    },
    {
      texto: 'Dinamicos',
      ruta: './reactive/dinamicos',
    },
    {
      texto: 'Switches',
      ruta: './reactive/switches',
    },
  ];

  public AuthMenu: MenuItem[] = [
    { texto: 'Registro', ruta: './auth/registro' },
    { texto: 'Login', ruta: './auth/login' },
  ];
}
