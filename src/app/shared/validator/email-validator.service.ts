import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  ValidationErrors,
} from '@angular/forms';
import { Observable } from 'rxjs';
import { map, delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EmailValidatorService implements AsyncValidator {
  constructor(private _http: HttpClient) {}

  validate(
    control: AbstractControl<any, any>
  ): Observable<ValidationErrors | null> {
    const email = control.value;
    console.log(email);
    return (
      this._http
        .get<any[]>(`http://localhost:3000/usuarios?q=${email}`)
        // el map nos permite cambiar el valor que esta retornando el observable y poner cualquier retorno y se pasa por un pipe
        .pipe(
          // nos permite poner un tiempo antes de que emita algun valor
          // antes de enviar la respuesta del map el servicio va a demorar 3 segundos
          //delay(3000),
          map((response) => {
            return response.length === 0 ? null : { emailTomado: true };
          })
        )
    );
  }
}
