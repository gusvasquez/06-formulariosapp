import { FormControl } from '@angular/forms';

export const nombreApellidoParent: string = '([a-zA-Z]+) ([a-zA-Z]+)';
export const emailPattern: string = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';

// VALIDACION PERSONALIZADA
export const noPuedeSerStrider = (argu: FormControl) => {
  const valor = argu.value?.trim().toLowerCase();
  if (valor == 'strider') {
    // error
    return {
      noStrider: true,
    };
  }
  // si una validacion retorna valor null esta bien la validacion
  return null;
};
