import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';
import {
  emailPattern,
  nombreApellidoParent,
  noPuedeSerStrider,
} from 'src/app/shared/validator/validaciones';
import { ValidatorService } from '../../../shared/validator/validator.service';
import { EmailValidatorService } from '../../../shared/validator/email-validator.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group(
    {
      nombre: [
        '',
        [
          Validators.required,
          Validators.pattern(this._validatorService.nombreApellidoParent),
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(this._validatorService.emailPattern),
        ],
        // validacion asincrona con peticion http
         [ this._emailValidator]
      ],
      // USERNAME CON VALIDACION PERSONALIZADA
      username: [
        '',
        [Validators.required, this._validatorService.noPuedeSerStrider],
      ],
      password: ['', [Validators.required, Validators.minLength(6)]],
      password2: ['', [Validators.required]],
    },
    {
      validators: [
        this._validatorService.camposIguales('password', 'password2'),
      ],
    }
  );

  get emailErrorMsg(): string{
    const emailErrors = this.miFormulario.get('email')?.errors;
    if(emailErrors?.['required']){
      return 'Email es obligatorio';
    }else if(emailErrors?.['pattern']){
      return 'El valor no tiene formato de correo';
    }else if(emailErrors?.['emailTomado']){
      return 'El correo ya existe';
    }
    return '';
  }

  constructor(
    private fb: FormBuilder,
    private _validatorService: ValidatorService,
    private _emailValidator: EmailValidatorService
  ) {}

  ngOnInit(): void {
    this.miFormulario.reset({
      nombre: 'Gustavo Vasquez',
      email: 'test1@test.com',
      username: 'Andres3636',
      password: '123456',
      password2: '123456'
    });
  }

  campoNoValido(campo: string) {
    return (
      this.miFormulario.get(campo)?.invalid &&
      this.miFormulario.get(campo)?.touched
    );
  }

  submitFormulario() {
    // dispara que todos los inputs fueran tocados para realizar las validaciones
    console.log(this.miFormulario.value);
    this.miFormulario.markAllAsTouched();
  }

 /*  emailRequired(){
    return (
      this.miFormulario.get('email')?.errors?.['required'] &&
      this.miFormulario.get('email')?.touched
    );
  }

  emailFormato(){
    return (
      this.miFormulario.get('email')?.errors?.['pattern'] &&
      this.miFormulario.get('email')?.touched
    );
  }
  emailTomado(){
    return (
      this.miFormulario.get('email')?.errors?.['emailTomado'] &&
      this.miFormulario.get('email')?.touched
    );
  } */

}
