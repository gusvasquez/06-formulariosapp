import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styleUrls: ['./switches.component.css']
})
export class SwitchesComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    genero: ['M', Validators.required],
    notificaciones: [ true, Validators.required],
    // requiero true
    condiciones: [ false, Validators.requiredTrue]
  });

  persona = {
    genero: 'F',
    notificaciones: true
  }

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    // operador expre para obtener todo lo que la persona tiene
    this.miFormulario.reset({...this.persona, condiciones: true});

    // otra manera de sincronizar pero desetructurando los objetos del formulario
    // solo se obtiene los datos que estan en restoDeArgumentos
   /*  this.miFormulario.valueChanges.subscribe(({condiciones,...restoDeArgumentos}) =>{
      // se elimina las posiciones que no se quieren poner
      //delete newForm.condiciones;
      this.persona = rest;
    }); */

    // sincronizar el formulario con el objeto persona
    this.miFormulario.valueChanges.subscribe(newForm =>{
      // se elimina las posiciones que no se quieren poner
      delete newForm.condiciones;
      this.persona = newForm;
    });
  }

  guardar(){
    // sacar copia del formulario
    const formValue = {...this.miFormulario.value};
    // eliminar una posciion del objeto del formulario
    delete formValue.condiciones;


    this.persona= formValue;

    console.log(formValue);
  }

}
