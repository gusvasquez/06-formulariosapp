import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styleUrls: ['./basicos.component.css']
})
export class BasicosComponent implements OnInit {

  /* miFormulario: FormGroup = new FormGroup({
    nombre: new FormControl('RTX 4080TI'),
    precio: new FormControl(0),
    existencias: new FormControl(5),
  }); */

  miFormulario: FormGroup = this.fb.group({
    nombre: [, [Validators.required, Validators.minLength(3)]],
    precio: [, [Validators.required, Validators.min(0)]],
    existencias: [, [Validators.required, Validators.min(0)]]
  });

  constructor(private fb: FormBuilder) { }

  campoNoValido(campo: string){
    return this.miFormulario.controls[campo].errors && this.miFormulario.controls[campo].touched;
  }

  ngOnInit(): void {
    // inicializar valores al formulario, reset es mejor para establecer valores al formulario
    this.miFormulario.reset({
      nombre: 'Rtx40801',
      precio: 1000
    })
  }

  guardar(){
    if(this.miFormulario.invalid){
      // para marcar los campos del formulario como tocados para realizar la validacion de los mismos
      this.miFormulario.markAllAsTouched();
      return;
    }
    console.log(this.miFormulario.value);
    this.miFormulario.reset();
  }

}
