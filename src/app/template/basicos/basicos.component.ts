import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styleUrls: ['./basicos.component.css']
})
export class BasicosComponent implements OnInit {

  // para obtener un atributo del formulario
  @ViewChild('miFormulario') miFormulario!: NgForm;

  constructor() { }

  ngOnInit(): void {
  }

  nombreValido(): Boolean {
    return this.miFormulario?.controls['producto']?.invalid && this.miFormulario?.controls['producto']?.touched;
  }

  precioValido(): Boolean {
    return this.miFormulario?.controls['precio']?.touched && this.miFormulario?.controls['precio']?.invalid;
  }

  existenciaValido(): Boolean {
    return this.miFormulario?.controls['existencias']?.touched && this.miFormulario?.controls['existencias']?.invalid;
  }

  guardar() {
    console.log('guardar', this.miFormulario);

    this.miFormulario.resetForm({
      producto: 'Nuevo Producto',
      precio: 0,
      existencias: 0
    }
    );
  }

}
