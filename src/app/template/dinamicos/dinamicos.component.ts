import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

interface Persona {
  nombre: string;
  favoritos: Favoritos[];
}

interface Favoritos {
  id: number;
  nombre: string;
}


@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styleUrls: ['./dinamicos.component.css']
})
export class DinamicosComponent {

  public persona: Persona = {
    nombre: 'Gustavo',
    favoritos: [
      { id: 1, nombre: 'Metal Gear' },
      { id: 2, nombre: 'Metal Gear2' }
    ]
  }

  public nuevoJuego: string = '';

  guardar() {

  }
  // agregar nuevo
  agregar() {
    const favorito: Favoritos = {
      // agrega this.persona.favoritos.length+1 el index que sigue en el arreglo de favoritos
      id: this.persona.favoritos.length + 1,
      nombre: this.nuevoJuego
    }
    this.persona.favoritos.push(favorito);
    this.nuevoJuego='';
  }

  eliminar(i: number) {
    // splice sirve para borrar elementos de un array se le indica la posicion a eliminar y la cantidad de elementos a borrar
    this.persona.favoritos.splice(i, 1);
  }

}
